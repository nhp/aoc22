package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	t, err := os.ReadFile("02/input.data")
	if err != nil {
		fmt.Println(err)
	}
	var total = 0
	rounds := strings.Split(string(t), "\n")
	for _, round := range rounds {
		s := strings.Fields(round)
		total = total + getPointsB(s[0], s[1])
		fmt.Println(total)

	}
	fmt.Println(total)
}
func getPointsB(a string, b string) int {
	var result = 0
	var c string
	c = toChoose(a, b)
	switch c {
	case "X":
		result = result + 1
	case "Y":
		result = result + 2
	case "Z":
		result = result + 3
	}
	// x = loose, y = draw, z = win
	if b == "Y" {
		result = result + 3
	} else if b == "Z" {
		result = result + 6
	}
	return result
}
func toChoose(a string, r string) string {
	var c string
	if (a == "A" && r == "X") || (a == "B" && r == "Z") || (a == "C" && r == "Y") {
		c = "Z"
	} else if (a == "B" && r == "X") || (a == "C" && r == "Z") || (a == "A" && r == "Y") {
		c = "X"
	} else {
		c = "Y"
	}
	fmt.Printf("A: %s, B: %s, Result: %s\n", a, c, r)
	return c
}

func getPointsA(a string, b string) int {
	var result = 0
	switch b {
	case "X":
		result = result + 1
	case "Y":
		result = result + 2
	case "Z":
		result = result + 3
	}
	if (a == "A" && b == "X") || (a == "B" && b == "Y") || (a == "C" && b == "Z") {
		result = result + 3
	} else if (a == "A" && b == "Y") || (a == "B" && b == "Z") || (a == "C" && b == "X") {
		result = result + 6
	} else {
		//draw
	}
	return result
}
