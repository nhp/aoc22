package main

import (
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	t, err := os.ReadFile("01/input.data")
	if err != nil {
		fmt.Println(err)
	}
	c := strings.Split(string(t), "\n\n")
	biggest := 0
	var calories []int
	// get the biggest amount of calories for first challenge
	for _, elve := range c {
		sum := 0
		for _, single := range strings.Split(elve, "\n") {
			cal, _ := strconv.Atoi(single)
			sum = sum + cal
		}
		calories = append(calories, sum)

		if sum > biggest {
			biggest = sum
		}
	}
	fmt.Printf("highest calorie amount: %d\n", biggest)
	sort.Sort(sort.Reverse(sort.IntSlice(calories)))
	fmt.Printf("Top 3 combined calories: %d\n", calories[0]+calories[1]+calories[2])
	// get the top 3 biggest for second challenge

}
