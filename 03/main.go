package main

import (
	"fmt"
	"os"
	"strings"
)

func main() {
	t, err := os.ReadFile("03/input.data")
	if err != nil {
		fmt.Println(err)
	}
	//	var priorities = 0
	items := strings.Split(string(t), "\n")
	priorities := 0
	for _, item := range items {

		priorities += getPrio(getDoublettes(item))
	}
	fmt.Println(priorities)
}
func getPrio(s string) int {
	runes := []rune(s)
	// ASCII code `a`
	cor := 96

	if (int(runes[0]) < 97) {
		cor = 38
	}
	return int(runes[0]) - cor
}

func getDoublettes(s string) string {
	res := ""
	if len(s) == 0 {
		return res
	}
	mid := len(s) / 2
	s1 := s[:mid]
	s2 := s[mid:]
	for _, c := range s1 {
		if strings.Count(s2, string(c)) > 0 {
			res = string(c)
		}
	}
	return res
}
